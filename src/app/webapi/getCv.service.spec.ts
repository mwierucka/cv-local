import { TestBed, inject } from '@angular/core/testing';

import { GetCVService } from './getCv.service';

describe('GetCVService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [GetCVService]
    });
  });

  it('should be created', inject([GetCVService], (service: GetCVService) => {
    expect(service).toBeTruthy();
  }));
});
