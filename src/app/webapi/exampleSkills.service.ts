import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ExampleSkillsService {

  constructor() { }

  getExampleSkills() {
    return exampleSkills;
  }

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~P O R Z Ą D E K~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

  // getSuggestions() {
  
  // }

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
}


const exampleSkills = [
  {
    name: 'Java',
    value: 0
  },
  {
    name: 'Perl',
    value: 0
  },
  {
    name: 'JavaScript',
    value: 0
  },
  {
    name: 'C#',
    value: 0
  },
  {
    name: 'C++',
    value: 0
  },
  {
    name: 'SQL',
    value: 0
  },
  {
    name: 'Ruby',
    value: 0
  }
  ]