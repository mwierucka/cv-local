import { Component, Input } from '@angular/core';
import { ISkill } from '../tile/skill.model';
import { SkillsService } from '../skills.service';

@Component({
  selector: 'main-tile',
  templateUrl: './main-tile.component.html',
  styleUrls: ['./main-tile.component.css'],
})
export class MainTileComponent {

  constructor(private skillsService: SkillsService) {}

  @Input() addedSkill: ISkill;
  @Input() fromSis: boolean;

  userInput: string;

  clearInput() {
    this.userInput = "";
  }

  rate() {
    console.log(this.userInput);
    }

  delete() {
    this.skillsService.deleteFromAdded(this.addedSkill);
  }

}
