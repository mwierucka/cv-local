import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import 'hammerjs';

import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from './material/material.module';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';

import { MatIconModule } from '@angular/material/icon';
import { SkillsComponent } from './skills/skills.component';
import { TileComponent } from './skills/tile/tile.component'; 
import { GetCVService } from './webapi/getCv.service';
import { SkillsService } from './skills/skills.service';
import { MainTileComponent } from './skills/main-tile/main-tile.component';


@NgModule({
  declarations: [
    AppComponent,
    SkillsComponent,
    TileComponent,
    MainTileComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MaterialModule,
    FormsModule, 
    ReactiveFormsModule,
    MatIconModule
  ],
  providers: [ GetCVService, SkillsService ],
  bootstrap: [AppComponent]
})
export class AppModule { }
